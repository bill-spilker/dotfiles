#!/bin/bash

# Installing Brew
echo "Installing Brew and Cask"
echo "Installing Brew"
yes '' | /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
echo "Installing Cask"
brew install cask
echo "Brew and Cask Installed"

echo
echo

# Installing applications using brew
declare -a programs=(
    "1password"
    "alfred"
    "bartender"
    "bitbar"
    "google-chrome"
    "iterm2"
    "itsycal"
    "jetbrains-toolbox"
    "slack"
    "spotify"
    "visual-studio-code"
    )

for p in "${programs[@]}"
do
  echo
  echo Installing $p
  brew cask install $p
done
