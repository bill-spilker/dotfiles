#!/usr/local/bin/bash

#
# <bitbar.title>BitcoinCash</bitbar.title>
# <bitbar.version>v1.1</bitbar.version>
# <bitbar.author>Monte Ohrt</bitbar.author>
# <bitbar.author.github>mohrt</bitbar.author.github>
# <bitbar.desc>Simply shows the value of BitcoinCash in USD</bitbar.desc>
# <bitbar.image>https://raw.githubusercontent.com/mohrt/bitbar-bitcoincash/master/bitcoin-cash.png</bitbar.image>
# <bitbar.abouturl>https://github.com/mohrt/bitbar-bitcoincash</bitbar.abouturl>
#
# Text above --- will be cycled through in the menu bar,
# whereas text underneath will be visible only when you
# open the menu.
#

PATH=/usr/local/bin:$PATH

bitcoin_icon='iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAQAAABLCVATAAAACXBIWXMAABYlAAAWJQFJUiTwAAABY0lEQVRIx2P4z0AdyEBzg1DAdIYfQJgCZHmCWdsYMAFRBs0BC2UAWT5g1p6hbZAggwIcrgALVQNZSWDWAQY24g3qwRtJ/xgeMqxkCGJgotQgGLzAoEUdg/4zvGQQIxzYLAyODF/gQv0MlgwWDK4MOQxbgV5DKG0nLtZ2wIUykII2EMmoU8QZtAWrQQwMB+HiDygzaDNc/CQlBskwfIKLN5JrkAxDFsMTuOh9BiFSDXoHDI2HDB9RlJ1kECc2r20hkI5OMXhQxyAQzCTNoDJgaAgAvaLLEMkwn+EbkuLvDBLkR78yUoD/Z0gn3yAGhnwk5V2UGBRGLYNmICkvIGzQLqwG8TA0oJQAVvgMymcoYehg+AUXWgoM0kygWC/DbpQ4+89wjYERt0FiRNeNX4GlFJ505EykMacZDPGn7HwCBnxiOMcwjcGJcOEvzqADh2vBQk1AVhaYdZCBc7TKpqJBA9ZiAwDMH49EXcmY2QAAAABJRU5ErkJggg=='

bch=$(curl -s https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/ | grep price_usd | cut -f4 -d '"')
bcc=$(curl -s https://api.coinmarketcap.com/v1/ticker/bitcoin/ | grep price_usd | cut -f4 -d '"')

bchOwned=1.1485

printf "BCH $%'0.2f\n" $bch
echo "---"
echo "Crypto Market Overview | href=https://coinmarketcap.com/"
echo "Hashrate | href=https://fork.lol/pow/hashrate"
printf "BCH $%'0.2f\n" $bch
printf "BCC $%'0.2f\n" $bcc
echo "$bch*$bchOwned" | bc
