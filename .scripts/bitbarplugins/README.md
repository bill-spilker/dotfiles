# Bitbar Plugins
This repo is the home to all of the bitbar scripts I use.

## Summary
Bitbar will execute scripts and then show the results in the macOS menu bar. Whatever scripts are placed in the `bitbarplugins` directory will display in the macOS menu bar.

[Bit Bar Website](https://getbitbar.com/)

This website has good resources to community made plugins. These are helpful for finding plugins for Bit Bar and are easy to edit to add my own creative touch.

## List of plugins
- Now Playing
- Bit Coin Cash Ticker

### Now Playing (Active)
This script will show the current song that is playing in either Spotify or Apple Music. It will also give play back controls for whatever applications is playing music. When no music source is playing music, it will give the option to open either Spotify or Apple Music

### Bit Coin Cash Ticker (Inactive)
This script will show the current price of BCH in the menu bar will BCC displaying in the menu. 