"---------------------------------------------------------------------
" Options

" Turn on syntax highlighting
syntax on

" Turn on line numbers
set number

" Always displays file name on the bottom status line
set laststatus=2

" Enable Mouse use everywhere
set mouse=a

" Highlight search result
set hlsearch

" Show matching bracket when text indicator is over them
set showmatch

"---------------------------------------------------------------------

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8
