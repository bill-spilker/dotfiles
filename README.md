# .dotfiles
These are the files that make my computer mine. 

## Summary
This repo contains all of my bash, git, vim, and other miscellaious scripts.
In this repo I also have scripted to download many of the apps I use on my computer.
Below are instructions on how to set this all up on a new computer.

## Insturctions

Download repository to your home directory `~`

Then execute the `setupDotfiles.sh` file
- This will symlink the correct files into the home directory
- This will symlink the correct directories into the home directory
- This will mark the global .gitignore file

Then execute the `installApplications.sh` file and install 
brew and the following programs via brew
- 1password
- alfred
- bartender
- bitbar
- google-chrome
- iterm2
- itsycal
- jetbrains-toolbox
- slack
- spotify
- visual-studio-code



