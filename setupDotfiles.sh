#!/bin/bash

# Files to symlink
declare -a files=(
    ".aliases" 
    ".bash_profile" 
    ".bash_prompt" 
    ".bashrc"
    ".git-completion.bash"
    ".gitconfig"
    ".gitignore_global"
    ".hyper.js"
    ".vimrc"
    )

for file in "${files[@]}"
do
  rm ~/$file
  ln -s ~/.dotfiles/$file ~/$file
  ls -l ~/.dotfiles/$file ~/$file
done

# Directories to symlink
rm -rf ~/.scripts
ln -s ~/.dotfiles/.scripts/ ~/.scripts

# Setting up a global .gitignore file
git config --global core.excludesfile ~/.gitignore_global